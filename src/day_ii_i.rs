use std::collections::HashMap;

pub fn day_ii_i(file_content: String) -> usize {
    let outcomes: [[usize; 3]; 3] = [[3, 0, 6],
                                     [6, 3, 0],
                                     [0, 6, 3]];
    let me_map: HashMap<&str, usize> = HashMap::from([("X", 0), ("Y", 1), ("Z", 2)]);
    let opp_map: HashMap<&str, usize> = HashMap::from([("A", 0), ("B", 1), ("C", 2)]);
    let turns = file_content.trim().split("\n");
    let mut total: usize = 0;
    for turn in turns {
        let moves: Vec<&str> = turn.split(" ").collect();
        total += me_map.get(moves[1]).unwrap() + 1;
        total += outcomes[*me_map.get(moves[1]).unwrap()][*opp_map.get(moves[0]).unwrap()];
    }
    total
}
