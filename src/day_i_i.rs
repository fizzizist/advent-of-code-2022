pub fn get_elf_cals(elf_str: &str) -> u32 {
    let elf_raw = elf_str.split("\n");
    let mut elf_cals = Vec::new();
    for e in elf_raw {
        elf_cals.push(e.parse::<u32>().unwrap());
    }
    elf_cals.iter().sum()
}

pub fn day_i_i(file_content: String) -> u32 {
    let elves_raw = file_content.split("\n\n");
    let mut highest: u32 = 0;
    for e in elves_raw {
        let elf_cal: u32 = get_elf_cals(e.trim());
        if elf_cal > highest {
            highest = elf_cal;
        }
    }
    highest
}
