use crate::day_i_i::get_elf_cals;

fn get_lowest_idx(arr: &mut [u32]) -> usize {
    let mut lowest_idx: usize = 0;
    for i in 1..arr.len() {
        if arr[i] < arr[lowest_idx] {
            lowest_idx = i;
        }
    }
    lowest_idx
}

pub fn day_i_ii(file_content: String) -> u32 {
    let elves_raw = file_content.split("\n\n");
    let mut highest: [u32; 3] = [0, 0, 0];
    let mut lowest_idx: usize = 0;
    for e in elves_raw {
        let elf_cal: u32 = get_elf_cals(e.trim());
        if elf_cal > highest[lowest_idx] {
            highest[lowest_idx] = elf_cal;
            lowest_idx = get_lowest_idx(&mut highest);
        }
    }
    highest.iter().sum()
}
