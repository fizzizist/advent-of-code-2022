use std::env;
use std::fs;

mod day_i_i;
use day_i_i::day_i_i;
mod day_i_ii;
use day_i_ii::day_i_ii;
mod day_ii_i;
use day_ii_i::day_ii_i;

fn main() {
    let args: Vec<String> = env::args().collect();
    let day = &args[1];
    let file_path = &args[2];

    let file_content = fs::read_to_string(file_path).expect("Unable to read input file!");

    match day.as_str() {
        "i-i" => println!("{}", day_i_i(file_content)),
        "i-ii" => println!("{}", day_i_ii(file_content)),
        "ii-i" => println!("{}", day_ii_i(file_content)),
        _ => println!("Day {} not found.", day),
    }
}
